<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $password = UserHandler::encodePassword("admin");
        $user = new User();
        $user
            ->setUsername("admin")
            ->setPassword($password)
            ->setEmail("admin@gmail.com")
            ->addRole("ROLE_ADMIN");

        $manager->persist($user);
        $manager->flush();
    }
}