<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends Controller
{
    /**
     * @Route("/admin_authentication", name="admin_authentication")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function authenticationAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler
    )
    {
        $form = $this->createForm("App\Form\LoginType");

        $form->handleRequest($request);

        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $username = $data['username'];
            $password = $data['password'];

            /** @var User $user || null */
            $user = $userRepository->findByPasswordAndUsername($username, $password);

            if ($user) {
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('homepage');
            } else {
                $error = 'Ошибка!Несуществующие данные!';
            }
        }
        return $this->render('authentication.html.twig', array(
            "form" => $form->createView(),
            'error' => $error
        ));
    }

}